#alias
source $HOME/.config/zsh/alias.zsh
#functions
source $HOME/.config/zsh/function.zsh
#zsh-plugins
source $HOME/.config/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $HOME/.config/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source $HOME/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#zsh-prompt
source $HOME/.config/zsh/spaceship-prompt.zsh


## Options section
unsetopt correct        
setopt extendedglob     
setopt nocaseglob       
setopt rcexpandparam    
setopt nocheckjobs      
setopt numericglobsort  
setopt nobeep           
setopt appendhistory    
setopt histignorealldups
setopt autocd           
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus


# Completion.
autoload -U compaudit compinit
compinit
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' rehash true                              # automatically find new executables in path
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' menu select=2
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*:descriptions' format '%U%F{cyan}%d%f%u'
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zcache

zstyle ':completion:*:git-checkout:*' sort false
zstyle ':completion:*:descriptions' format '[%d]'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
zstyle ':fzf-tab:*' switch-group ',' '.'

autoload -U +X bashcompinit && bashcompinit
HISTFILE=~/.zhistory
HISTSIZE=50000
SAVEHIST=10000

autoload -U url-quote-magic bracketed-paste-magic
zle -N self-insert url-quote-magic
zle -N bracketed-paste bracketed-paste-magic


