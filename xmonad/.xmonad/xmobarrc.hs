Config { font              =   "xft:JetBrainsMono Nerd Font:pixelsize=12"

       , additionalFonts   = [ "xft:3270SemiNarrow Nerd Font Mono:pixelsize=33"
                             , "xft:3270SemiNarrow Nerd Font Mono:pixelsize=38"
                             , "xft:Meslo LG S DZ:pixelsize=20:style=Bold"
                             , "xft:Meslo LG S DZ:pixelsize=6"
                             , "xft:3270SemiNarrow Nerd Font Mono:pixelsize=21"
                             , "xft:3270SemiNarrow Nerd Font Mono:pixelsize=25"
                             , "xft:Meslo LG S DZ:pixelsize=22:style=Bold"
                             , "xft:3270SemiNarrow Nerd Font Mono:pixelsize=28"

                             ]
       , bgColor           =   "#282c34"
       , fgColor           =   "#ff6c6b"
       , alpha             =   255
       , position          =   Static { xpos = -10 , ypos = 0, width = 1940, height = 30 }
       , lowerOnStart      =   True
       , hideOnStart       =   False
       , allDesktops       =   True
       , overrideRedirect  =   False
       , persistent        =   True

       , commands          = [ Run UnsafeStdinReader

                             , Run BatteryP	["BAT1"]
                                           	[ "-t"      , "<acstatus>"
										   	, "--Low"   , "10"
										   	, "--High"  , "85"
										   	, "--low"   , "#ff0000"
										   	, "--high"  , "#00ff00"
										   	, "--"
										   	, "-O"      , "<fc=#47c97b><fn=1>\xf584</fn><fn=4> </fn><fn=3><left>%</fn></fc>"
										   	, "-o"      , "<fc=#9fd6b4><fn=5>\xf57c</fn><fn=4> </fn><fn=3><left>%  <fc=#cbd2df><fn=1>ﴔ<fn=4> </fn></fn></fc><fn=3><fc=#C778DD>[</fc><fc=#A2CD83>[</fc><fc=#cbd2df><timeleft></fc><fc=#A2CD83>]</fc><fc=#C778DD>]</fc></fn></fn></fc>"
										   	, "-i"      , "<fc=#00ff00>Charged</fc>"
										   	, "-A"      , "5"
										   	, "-a"      , "notify-send -u critical 'Battery running out!!'" ] 30

                             , Run Cpu [ "-t"      , "<fn=1>\xf135</fn><fn=4> </fn><fn=3><total>%</fn>"
                                       , "-H"      ,"40"
                                       , "--high"  ,"#ff3333" ] 10

                             , Run ThermalZone 0 [ "-t"      , "<fc=#EFCA84><fn=6>\xf2c9</fn><fn=4> </fn><fn=3><temp>°C</fn></fc>"
                                                 , "--High"  , "60"
                                                 , "--high"  , "#ff3333" ] 10

                             , Run Date "<fn=5>\xfa1e</fn><fn=4> </fn><fn=3>%I<fn=7>:</fn>%M%p</fn>" "date" 600

                             , Run Brightness [ "-t", "<percent>%"
                                              , "--"
                                              , "-D", "intel_backlight" ] 3

                             , Run Volume  "default" "Capture"
                                         [ "-t", "<status>"
                                         , "--"
                                         , "-O", "<fn=8>\xf130</fn>"
                                         , "-o", "<fn=1>\xf131</fn>"
                                         ] 5
                             , Run Volume "default" "Master"
                                         [ "-t", "<status>"
                                         , "--"
                                         , "-O", "<fc=#dcffa8><fn=2>\xfa7d</fn><fn=4> </fn><fn=3><volume>%</fn></fc>"
                                         , "-o", "<fn=2>\xfa80</fn><fn=4> </fn><fn=3><volume>%</fn>"
                                         ] 5
                             ]
     
       , sepChar           = "%"
       , alignSep          = "}{"
       , template          = "   <fn=1>%UnsafeStdinReader%</fn> } <fc=#91b5e6>%date%</fc> { %default:Capture%    <action=`pactl set-sink-mute @DEFAULT_SINK@ toggle`>%default:Master%</action>    <fc=#D788ED><action=`light -S 30`><action=`light -U 2` button=5><action=`light -A 2` button=4><fn=2></fn><fn=4> </fn><fn=3>%bright%</fn></action></action></action></fc>    %thermal0%    <fc=#39dbc5>%cpu%</fc>    %battery%   "
    }
